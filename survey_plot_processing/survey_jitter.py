import os
import subprocess
import shutil
import psycopg2
from scipy.optimize import minimize 


def connect_to_DB():
    DB_HOST = "localhost"
    DB = ""
    DB_USER = "postgres"
    DB_PASSWORD = ""
    try:
        conn = psycopg2.connect(
        host = DB_HOST,
        database = DB,
        user = DB_USER,
        password = DB_PASSWORD)
        conn.autocommit = True
        print("DB Connected")
        return conn.cursor()
    except:
        print("Connection failed")


def translate_survey_no():
    sql = f'''
        drop table if exists {survey_shifted};
        create table {survey_shifted} as select * from {survey_original};
        with delta as (
        select st_x(st_centroid(st_union(p.geom))) - st_x(st_centroid(st_union(q.geom))) as dx,
        st_y(st_centroid(st_union(p.geom))) - st_y(st_centroid(st_union(q.geom))) as dy
        from {village}.cadastrals as p, {survey_original} as q 
    )
    update {survey_shifted} 
    set geom = st_translate(geom,(select dx from delta),(select dy from delta));
    '''
    curr.execute(sql)

def a_minus_b(w):
    sql = f'''
        with center as (
        select st_centroid(st_union(geom)) as geom from {survey_shifted})
        , rotated as (
            select st_rotate(p.geom,{w[0]},c.geom) as geom from center as c, {survey_shifted} as p
        )
        , scaled as (
            select st_scale(p.geom,st_makePoint({w[1]},{w[2]}),c.geom) as geom from center as c,
            rotated as p
        )
         select st_area(
                st_difference(
                    (select st_union(geom) from {village}.cadastrals where PIN is not null ),
                    (select st_translate((select st_union(geom) from scaled),{w[3]},{w[4]}))
                    
                )
            );
        '''
    curr.execute(sql)
    
    a = curr.fetchall()
    return float(a[0][0])

def b_minus_a(w):
    sql = f'''
        with center as (
        select st_centroid(st_union(geom)) as geom from {survey_shifted})
        , rotated as (
            select st_rotate(p.geom,{w[0]},c.geom) as geom from center as c, {survey_shifted} as p
        )
        , scaled as (
            select st_scale(p.geom,st_makePoint({w[1]},{w[2]}),c.geom) as geom from center as c,
            rotated as p
        )
         select st_area(
                st_difference(
                    (select st_translate((select st_union(geom) from scaled),{w[3]},{w[4]})),
                    (select st_union(geom) from {village}.cadastrals where PIN is not null ) 
                )
            );
        '''
    curr.execute(sql)
    a = curr.fetchall()
    return float(a[0][0])


def excess_area(w):
    return a_minus_b(w) + b_minus_a(w)


def update_scale(sx,sy):
    sql = f'''
        drop table if exists {survey_scaled_rotated};
        create table {survey_scaled_rotated} as
        select * from {survey_shifted};
        with c as (
        select st_centroid(st_union(geom)) as geom from {survey_scaled_rotated}
    )
    update {survey_scaled_rotated}
    set geom = (select st_scale({survey_scaled_rotated}.geom,st_makePoint({sx},{sy}),c.geom) from c);
    '''
    curr.execute(sql)

def update_rotation(r,x,y):
    sql = f'''
        with c as (
            select st_centroid(st_union(geom)) as geom from {survey_scaled_rotated}
        )
        update {survey_scaled_rotated}
        set geom = (select st_rotate({survey_scaled_rotated}.geom, {r}, c.geom) from c);
        update {survey_scaled_rotated}
        set geom = st_translate(geom,{x},{y})
    '''
    curr.execute(sql)



def fitting():
    result = minimize(excess_area,[0,10,6,0,0],args=(),bounds=bnds)
    print(result)
    update_scale(result.x[1],result.x[2])
    update_rotation(result.x[0],result.x[3],result.x[4])


village = 'sawangi'
curr = connect_to_DB()
bnds = ((-1,1),(0.01,15),(0.01,15),(-50,50),(-50,50))
farm_plot = f'{village}.farmplots'
survey_original = f'{village}.survey_original'
survey_shifted = f'{village}.survey_shifted'
survey_scaled_rotated = f'{village}.survey_scaled_trans_rotated'
regions = f'{village}.regions'


# The shape file given by DoLR has been loaded into DB as survey_original.

# survey_original is the input to the algo. 

# survey_shifted refers to
# shifting survey polygons to the centroid of cadastral union
# done by translate_survey_no()

# survey_scaled_rotated refers to jitter fit on survey_shifted to fit cadastrals
# done by fitting()

# create_regions() divides the village into multiple regions based on river boundaries 

# translate_survey_no()
# fitting()
# create_regions()