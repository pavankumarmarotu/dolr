from scipy.optimize import minimize 
import psycopg2

def connect_to_DB():
    DB_HOST = "localhost"
    DB = "govt"
    DB_USER = "postgres"
    DB_PASSWORD = "cooking"
    try:
        conn = psycopg2.connect(
        host = DB_HOST,
        database = DB,
        user = DB_USER,
        password = DB_PASSWORD)
        conn.autocommit = True
        print("DB Connected")
        return conn.cursor()
    except:
        print("Connection failed")
curr = connect_to_DB()

def add_region_no_column():
    sql = f'''alter table {survey_jitter}
            add column region_no int;'''
    curr.execute(sql)

def create_regions():
    sql = f'''
            drop table if exists {regions};
            create table {regions} as 
            with unions as (select st_union(geom) as geom from {survey_jitter}) 
            select (st_Dump(unions.geom)).geom as geom from unions;
            alter table {regions} add region_no serial;
            with r as (select p.survey_no,q.region_no from {survey_jitter} as p
                        inner join {regions} as q on
                        st_intersects(p.geom,q.geom))
            update {survey_jitter} set 
            region_no = ( select region_no from r where r.survey_no = {survey_jitter}.survey_no)'''
    curr.execute(sql)

def excess_area_farm_plots_regions(weights,i):
    x = weights[0]
    y = weights[1]
    r = weights[2]
    excess_area =f''' 
                select sum(least(st_area(st_difference(
                                    st_transform(P.geom,32643),
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                                        )),
                                st_area(st_intersection(
                                    st_transform(P.geom,32643), 
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                        %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0))
                                                            )))
                ))
                
                from {farm_plot} as P
                    inner join 
                    {regions} as Q
                    on
                    Q.region_no = '%(i)s'
                    and
                    ST_intersects(st_transform(P.geom,32643),
                                ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                    ) ;
                '''
    curr.execute(excess_area,{'x': x, 'y': y, 'r': r, 'i': i})
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))

def jitter():
    sql = f'''select count(geom) from {regions}; '''
    curr.execute(sql)
    number_of_regions = curr.fetchall()
    sql = f'''drop table if exists {survey_region_jitter};
            create table {survey_region_jitter} as select * from {survey_jitter} '''
    curr.execute(sql)
    for i in range(number_of_regions[0][0]):
        print(i)
        result = minimize(excess_area_farm_plots_regions,[0,0,0],args=(i+1),bounds=bnds2) 
        print(result.x)
        update_regions(float(result.x[0]), float(result.x[1]),i+1)   

def update_regions(x,y,i):
    sql = f'''update {survey_region_jitter} 
            set geom = st_translate(geom,{x},{y}) where region_no = {i} '''
    curr.execute(sql)

def create_farmplots_superpolygons():
    sql = f'''
            drop table if exists {farm_plot_superpolygons};
            create table {farm_plot_superpolygons} as
            with t as (
                select p.*, q.survey_no from
                {survey_region_jitter} as q inner join {village}.t1 as p
                on 
                st_area(st_intersection(st_transform(p.geom,32643),q.geom)) > 
                0.7*st_area(st_transform(p.geom,32643))
            )
            select st_buffer(st_union(st_buffer(st_transform(geom,32643),20)),-20) as geom 
            from t group by survey_no;
            '''
    curr.execute(sql)

def simplify():
    sql = '''
            create table sawangi.super_clean as 
            select st_simplifypreserveTopology(st_transform(geom,32643),25) 
            from sawangi.farmplots_superpolygons;
            '''



village = 'sawangi'
bnds2 = ((-20,20),(-20,20),(0,0))
farm_plot = f'{village}.farmplots'
farm_plot_superpolygons = f'{village}.farmplots_superpolygons'
survey_jitter = f'{village}.survey_jitter'
regions = f'{village}.regions'
survey_region_jitter = f'{village}.survey_region_jitter'

# add_region_no_column()
# create_regions()
# jitter()
create_farmplots_superpolygons()