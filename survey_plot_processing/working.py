import os
import subprocess
from scipy.optimize import minimize 
import pandas as pd
import connecting_to_db

curr = connecting_to_db

def translate_survey_no():
    sql = f'''
        drop table if exists {survey_shifted};
        create table {survey_shifted} as select * from {survey_original};
        with delta as (
        select st_x(st_centroid(st_union(p.geom))) - st_x(st_centroid(st_union(q.geom))) as dx,
        st_y(st_centroid(st_union(p.geom))) - st_y(st_centroid(st_union(q.geom))) as dy
        from {village}.cadastrals as p, {survey_original} as q 
    )
    update {survey_shifted} 
    set geom = st_translate(geom,(select dx from delta),(select dy from delta));
    '''
    curr.execute(sql)

def a_minus_b(w):
    sql = f'''
        with center as (
        select st_centroid(st_union(geom)) as geom from {survey_shifted})
        , rotated as (
            select st_rotate(p.geom,{w[0]},c.geom) as geom from center as c, {survey_shifted} as p
        )
        , scaled as (
            select st_scale(p.geom,st_makePoint({w[1]},{w[2]}),c.geom) as geom from center as c,
            rotated as p
        )
         select st_area(
                st_difference(
                    (select st_union(geom) from {village}.cadastrals where PIN is not null ),
                    (select st_translate((select st_union(geom) from scaled),{w[3]},{w[4]}))
                    
                )
            );
        '''
    curr.execute(sql)
    
    a = curr.fetchall()
    return float(a[0][0])

def b_minus_a(w):
    sql = f'''
        with center as (
        select st_centroid(st_union(geom)) as geom from {survey_shifted})
        , rotated as (
            select st_rotate(p.geom,{w[0]},c.geom) as geom from center as c, {survey_shifted} as p
        )
        , scaled as (
            select st_scale(p.geom,st_makePoint({w[1]},{w[2]}),c.geom) as geom from center as c,
            rotated as p
        )
         select st_area(
                st_difference(
                    (select st_translate((select st_union(geom) from scaled),{w[3]},{w[4]})),
                    (select st_union(geom) from {village}.cadastrals where PIN is not null ) 
                )
            );
        '''
    curr.execute(sql)
    a = curr.fetchall()
    return float(a[0][0])


def excess_area(w):
    return a_minus_b(w) + b_minus_a(w)


def update_scale(sx,sy):
    sql = f'''
        drop table if exists {survey_scaled_rotated};
        create table {survey_scaled_rotated} as
        select * from {survey_shifted};
        with c as (
        select st_centroid(st_union(geom)) as geom from {survey_scaled_rotated}
    )
    update {survey_scaled_rotated}
    set geom = (select st_scale({survey_scaled_rotated}.geom,st_makePoint({sx},{sy}),c.geom) from c);
    '''
    curr.execute(sql)

def update_rotation(r,x,y):
    sql = f'''
        with c as (
            select st_centroid(st_union(geom)) as geom from {survey_scaled_rotated}
        )
        update {survey_scaled_rotated}
        set geom = (select st_rotate({survey_scaled_rotated}.geom, {r}, c.geom) from c);
        update {survey_scaled_rotated}
        set geom = st_translate(geom,{x},{y})
    '''
    curr.execute(sql)

def add_region_no_column():
    sql = f'''alter table {survey_scaled_rotated}
            add column region_no int;'''
    curr.execute(sql)

def create_regions():
    sql = f'''
            drop table if exists {regions};
            create table {regions} as 
            with unions as (select st_union(geom) as geom from {survey_scaled_rotated}) 
            select (st_Dump(unions.geom)).geom as geom from unions;
            alter table {regions} add region_no serial;
            with r as (select p.survey_no,q.region_no from {survey_scaled_rotated} as p
                        inner join {regions} as q on
                        st_intersects(p.geom,q.geom))
            update {survey_scaled_rotated} set 
            region_no = ( select region_no from r where r.survey_no = {survey_scaled_rotated}.survey_no)'''
    curr.execute(sql)
    sql = f'''select count(geom) from {regions}'''
    curr.execute(sql)
    number_of_regions = curr.fetchall()
    print(number_of_regions[0][0])


def fitting():
    result = minimize(excess_area,[0,10,6,0,0],args=(),bounds=bnds)
    print(result)
    update_scale(result.x[1],result.x[2])
    update_rotation(result.x[0],result.x[3],result.x[4])


   
def create_union(union,polygons):
    sql =f''' drop table if exists {union};
        create table {union} as select st_union(geom) as geom from {polygons};
            '''
    curr.execute(sql)
def excess_area_farm_plots_union(weights):
    x = weights[0]
    y = weights[1]
    r = weights[2]
    excess_area =f''' 
                
               select sum(least(st_area(st_difference(
                                    st_transform(P.geom,32643),
                                    ST_rotate(ST_translate((select geom from {union}),%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate((select geom from {union}),%(x)s,%(y)s,0)))
                                                        )),
                                st_area(st_intersection(
                                    st_transform(P.geom,32643), 
                                    ST_rotate(ST_translate((select geom from {union}),%(x)s,%(y)s,0),
                                        %(r)s, ST_centroid(ST_translate((select geom from {union}),%(x)s,%(y)s,0))
                                                            )))
                ))
                
                from {farm_plot} as P
                '''
    curr.execute(excess_area,{'x': x, 'y': y, 'r': r})
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))


bnds2 = ((-200,100),(-200,200),(0,0))

def jitter_union():
    result = minimize(excess_area_farm_plots_union,[-10,-10,0],bounds=bnds2) 
    print(result)  

def update_jitter(x,y):
    sql = f'''drop table if exists {jitter_output};
            create table {jitter_output} as select * from {survey_scaled_rotated};
            update {jitter_output} 
            set geom = st_translate(geom,{x},{y}); '''
    curr.execute(sql)

def total_area():
    sql = f'''select st_area(st_union(geom)) from {village}.cadastrals'''
    curr.execute(sql)
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))

def get_all_survey_no():
    sql = f'''select survey_no from {survey_scaled_rotated}'''
    curr.execute(sql)
    sql_result = curr.fetchall()
    result =[]
    for i in range(len(sql_result)):
        a = sql_result[i][0]
        if(a == None or not a):
            result.append(-1)
        elif a.isdigit():
            assert(int(a) != 0)
            result.append(int(a))
        else:
            result.append(-1)
    result.sort()
    # print(result)s
    return result

def excess_area_farm_plots_ind(survey,weights,i):
    x = weights[0]
    y = weights[1]
    r = weights[2]
    excess_area =f''' 
                select sum(least(st_area(st_difference(
                                    st_transform(P.geom,32643),
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                                        )),
                                st_area(st_intersection(
                                    st_transform(P.geom,32643), 
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                        %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0))
                                                            )))
                ))
                
                from {farm_plot} as P
                    inner join 
                    {survey} as Q
                    on
                    Q.survey_no = '%(i)s'
                    and
                    ST_intersects(st_transform(P.geom,32643),
                                ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                    ) ;
                '''
    curr.execute(excess_area,{'x': x, 'y': y, 'r': r, 'i': i})
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))
    
def area_of_survey(survey,no):
    sql = f'''select st_area(geom) from {survey} where survey_no = '{no}' '''
    curr.execute(sql)
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))

def total_excess(survey):
    survey_no = get_all_survey_no()
    excess = 0
    total = 0
    for i in survey_no:
        if(i==-1):
            continue
        excess = excess + excess_area_farm_plots_ind(survey,[0,0,0],i)
        total = total + area_of_survey(survey,i)
    return (excess*100/total)

def individual_excess(survey):
    survey_no = get_all_survey_no()
    # print(survey_no)
    ls = []
    for i in survey_no:
        if(i==-1):
            continue
        excess = excess_area_farm_plots_ind(survey,[0,0,0],i)
        total = area_of_survey(survey,i)
        percent = excess*100/total
        l = [i,total,excess/2,percent]
        assert(l[0] != 0)
        ls.append(l)
    df = pd.DataFrame(ls,columns=['Survey Number','Area','Excess Area(Deviation)','Excess Percent'])
    df.to_excel(f'{village}.xlsx',index=False)


village = 'bramhanwada'
bnds = ((-1,1),(0.01,15),(0.01,15),(-50,50),(-50,50))
farm_plot = f'{village}.farmplots'
survey_original = f'{village}.survey_original'
survey_shifted = f'{village}.survey_shifted'
survey_scaled_rotated = f'{village}.survey_scaled_trans_rotated'
regions = f'{village}.regions'
union = f'{village}.union'
jitter_output = f'''{village}.survey_jitter'''
union2 = f'{village}.union_jitter'

# print(total_excess(jitter_output))
# create_union(union2,jitter_output)
# print(excess_area_farm_plots_union(union,[0,0,0])*100/total_area())
# print(excess_area_farm_plots_ind(jitter_output,[0,0,0],3))
# print(area_of_survey(jitter_output,2))
# individual_excess(jitter_output)
# # create_union()
jitter_union()
# update_jitter(17.60,-6.96)
# add_region_no_column()
# create_regions()

# fitting()
# translate_survey_no()


# The functions below have not been completed


path = "/home/abhishek/Downloads/Barhanpur_tippan_data/Barhanpur/Tippan"
table_name = "barhanpur_tippan2323"

def load_to_sql():
    i = 0
    for root, dirs, files in os.walk(path):
        for sub_folder in dirs:
            # print(sub_folder)
            for f in sub_folder:
                
                if f.endswith('.shp'):
                    i = i+1
                    file_path = os.path.join(path,sub_folder,f)
                    print(file_path)
                    if(i==1):
                        subprocess.run(["shp2pgsql","-I","-s","4326",file_path,table_name,"|","psql","-U","postgres","-d","govt"],shell=True)
                    else:
                        subprocess.run(["shp2pgsql","-a","-s","4326",file_path,table_name,"|","psql","-U","postgres","-d","govt"],shell=True)
        
def change_coordinate():
    new = "32643"
    old = "4326"
    sql_update = '''select UpdateGeometrySRID('public', 'barhanpur_tippan', 'geom', 32643)'''
    curr.execute(sql_update)

def create_bounding_box():
    polygon_table = '{village}.survey_no'
    sql = f'''
        create table bounding_box as 
        select st_expand(st_setSRID(st_extent(geom),32643),200) as geom 
        from {polygon_table};
    '''
    curr.execute(sql)


def make_valid():
    sql = '''
        update {} set geom = st_collectionextract(st_makevalid(geom),3) where
                st_isvalid(geom) is false;
                update {} set geom = st_collectionextract(st_makevalid(geom),3) where
                st_isvalid(geom) is false;
        '''

def excess_area_farm_plots2(weights,i):
    x = weights[0]
    y = weights[1]
    r = weights[2]
    excess_area =f''' 
                select sum(least(st_area(st_difference(
                                    st_transform(P.geom,32643),
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                                        )),
                                st_area(st_intersection(
                                    st_transform(P.geom,32643), 
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                        %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0))
                                                            )))
                ))
                
                from {farm_plot} as P
                    inner join 
                    {survey_shifted} as Q
                    on
                    Q.survey_no = '%(i)s'
                    and
                    ST_intersects(st_transform(P.geom,32643),
                                ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                    ) ;
                '''
    curr.execute(excess_area,{'x': x, 'y': y, 'r': r, 'i': i})
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))
    


def excess_area_farm_plots3(weights,i):
    x = weights[0]
    y = weights[1]
    r = weights[2]
    excess_area =f''' 
                select sum(least(st_area(st_difference(
                                    st_transform(P.geom,32643),
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                                        )),
                                st_area(st_intersection(
                                    st_transform(P.geom,32643), 
                                    ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                        %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0))
                                                            )))
                ))
                
                from {farm_plot} as P
                    inner join 
                    {regions} as Q
                    on
                    Q.region_no = '%(i)s'
                    and
                    ST_intersects(st_transform(P.geom,32643),
                                ST_rotate(ST_translate(Q.geom,%(x)s,%(y)s,0),
                                    %(r)s, ST_centroid(ST_translate(Q.geom,%(x)s,%(y)s,0)))
                                    ) ;
                '''
    curr.execute(excess_area,{'x': x, 'y': y, 'r': r, 'i': i})
    a = curr.fetchall()[0][0]
    if(a == None or not a):
        return 0
    else:
        return float(str(a))

def jitter():
    result = minimize(excess_area_farm_plots3,[-10,-10,0],args=(3),bounds=bnds2) 
    print(result)   

def update_regions(x,y):
    sql = f'''update {regions} 
            set geom = st_translate(geom,{x},{y}) where region_no = 3'''
    curr.execute(sql)
# jitter()

# update_rotation(0, -1.8409799,-64.42754259)
# update_regions(9.30840571,  -67.53559002)

def translate_tippans():
    sql = '''create table {village}.tippans2 as
            select p.survey_no,st_translate(p.geom,
                st_x(st_centroid(q.geom)) - st_x(st_centroid(p.geom)),
                st_y(st_centroid(q.geom)) - st_y(st_centroid(p.geom))
                )
            from {village}.tippans as p
            inner join
            {village}.survey as q
            on p.survey_no = q.survey_no'''


# {village}.survey_original is the input to the algo. 

# {village}.survey_shifted => 
# shifting survey polygons to the centroid of cadastral union
# done by translate_survey_no()

# {village}.survey_scaled_rotated => jitter fit on survey_shifted to fit cadastrals
# done by fitting()
