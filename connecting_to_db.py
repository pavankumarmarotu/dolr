import psycopg2


def connect_to_DB():
    DB_HOST = "localhost"
    DB = "govt"
    DB_USER = "postgres"
    DB_PASSWORD = "cooking"
    try:
        conn = psycopg2.connect(
        host = DB_HOST,
        database = DB,
        user = DB_USER,
        password = DB_PASSWORD)
        conn.autocommit = True
        print("DB Connected")
        return conn.cursor()
    except:
        print("Connection failed")

