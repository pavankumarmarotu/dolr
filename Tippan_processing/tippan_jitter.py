import psycopg2
from scipy.optimize import minimize
import pandas as pd
from psycopg2.extras import execute_batch

def connect_to_DB():
    DB_HOST = "localhost"
    DB = "govt"
    DB_USER = "postgres"
    DB_PASSWORD = "cooking"
    try:
        conn = psycopg2.connect(
            host=DB_HOST,
            database=DB,
            user=DB_USER,
            password=DB_PASSWORD)
        conn.autocommit = True
        print("DB Connected")
        return conn.cursor()
    except:
        print("Connection failed")


def find_diff(w,tippan_trans,survey_plots,survey_no):
    sql = f'''
    
    select st_area(st_difference
    (q.geom,ST_rotate(ST_translate(p.geom,{w[0]},{w[1]},0),
                                    {w[2]}, ST_centroid(ST_translate(p.geom,{w[0]},{w[1]},0)))))
            from {tippan_trans} as p
            inner join
            {survey_plots} as q
            on p.kide = '{survey_no}' and
            q.Survey_No = '{survey_no}' '''

    curr.execute(sql)
    a = curr.fetchall()[0][0]
    if (a == None or not a):
        return 0
    else:
        return float(a)


def other_intersection_area(w,tippan_trans,survey_plots, survey_no):
    sql = f'''select sum(
                    st_area(
                        st_intersection(
                            q.geom,ST_rotate(ST_translate(p.geom,{w[0]},{w[1]},0),
                                    {w[2]}, ST_centroid(ST_translate(p.geom,{w[0]},{w[1]},0)))
                        )
                    )
    ) 
    from {tippan_trans} as p
    inner join
    {survey_plots} as q
    on p.kide = '{survey_no}' and 
    q.survey_no != '{survey_no}'
    '''

    curr.execute(sql)
    a = curr.fetchall()[0][0]
    if (a == None or not a):
        return 0
    else:
        return float(a)


def excess_area(w,tippan_trans,survey_plots,survey_no):

    return find_diff(w,tippan_trans,survey_plots, survey_no) + other_intersection_area(w,tippan_trans,survey_plots,survey_no)


def find_survey_no(tippan_trans,survey_plots):
    sql = f'''select p.kide 
            from {tippan_trans} as p
            inner join
            {survey_plots} as q
            on p.kide = q.Survey_No;
            '''
    curr.execute(sql)
    result = []
    sql_result = curr.fetchall()
    for i in range(len(sql_result)):
        result.append(sql_result[i][0])
    return result


def new_cadastral_table():
    new_cadastral = f'''
                    drop table if exists {tippan_fixed};
                    create table {tippan_fixed} (like {tippan_trans});
                    alter table {tippan_fixed}
                    add column excess_area float,
                    add column cross_over float;
                    '''
    curr.execute(new_cadastral)


def add_to_new_cadastral_table(survey_no, w,excess_area,cross_over):
    add_to_new_cadastral_table = f''' insert into {tippan_fixed}
                                     select * from {tippan_trans}
                                     where kide = '{survey_no}';
                                     update {tippan_fixed}
                                     set geom = 
                                        ST_rotate(ST_translate(geom,{w[0]},{w[1]},0),
                                    {w[2]}, ST_centroid(ST_translate(geom,{w[0]},{w[1]},0)))
                                    where kide = '{survey_no}';
                                    update {tippan_fixed}
                                    set excess_area = {excess_area}
                                    where kide = '{survey_no}';
                                    update {tippan_fixed}
                                    set cross_over = {cross_over}
                                    where kide = '{survey_no}';
                                '''
    curr.execute(add_to_new_cadastral_table)


# def translate_tippans():
#     sql = '''create table barhanpur_tippan_trans2 as
#             select p.kide,st_translate(p.geom,
#                 st_x(st_centroid(q.geom)) - st_x(st_centroid(p.geom)),
#                 st_y(st_centroid(q.geom)) - st_y(st_centroid(p.geom))
#                 )
#             from barhanpur_tippan as p
#             inner join
#             barhanpur_survey_plots as q
#             on p.kide = q.Survey_No'''


def fix_tippans():
    sur_no = find_survey_no(tippan_trans,survey_plots)
    new_cadastral_table()
    before = 0
    after = 0
    i = 0
    for s in sur_no:
        print(i," out of ",len(sur_no)," fixed")
        i = i+1
        before = before + excess_area([0, 0, 0], tippan_trans,survey_plots,s)
        result = [
            minimize(excess_area,  [0, 0, 6.4*(x/n)], args=(tippan_trans,survey_plots,s), bounds = bnds,method='Nelder-Mead')
            for x in range(n)
        ]
        result.sort(key = lambda y:excess_area(y.x,tippan_trans,survey_plots,s))
        add_to_new_cadastral_table(s, result[0].x,excess_area(result[0].x,tippan_trans,survey_plots, s)/area_of(tippan_trans,s),other_intersection_area(result[0].x,tippan_trans,survey_plots, s))
        after = after + excess_area(result[0].x,tippan_trans,survey_plots, s)
    print("Excess area before ", before/10000)
    print("Excess area after  ", after/10000)

def area_of_survey(survey,s):
    sql = f'''select st_area(geom) 
            from {survey} where
            {survey}.Survey_No = '{s}' '''
    curr.execute(sql)
    ans = curr.fetchall()
    # print(ans[0][0])
    return float(ans[0][0])

def area_of(tippan,s):
    sql = f'''select st_area(geom) 
            from {tippan} where
            {tippan}.kide = '{s}' '''
    curr.execute(sql)
    ans = curr.fetchall()
    # print(ans[0][0])
    return float(ans[0][0])

def result_analysis():
    sur_no = find_survey_no(tippan_trans,survey_plots)
    res = []
    after = 0
    for s in sur_no:
        after = after + excess_area([0,0,0],tippan_fixed,survey_plots,s)
        error = 100*excess_area([0,0,0],tippan_fixed,survey_plots,s)/area_of(tippan_fixed,s)
        res.append([s,error])
    res.sort(key=lambda x:-x[1])
    sql = f'''drop table if exists {tippan_bad};
            create table {tippan_bad} (like {tippan_fixed})'''
    curr.execute(sql)
    for x in res:
        if x[1] > 5:
            print(x)
            sql = f''' insert into {tippan_bad} select * from 
                        {tippan_fixed} as p where p.kide = '{x[0]}' '''
            curr.execute(sql)
    
def area_diff():
    sur_no = find_survey_no(tippan_trans,survey_plots)
    res = []
    for s in sur_no:
        a = area_of(tippan_fixed,s)
        b = area_of_survey(survey_plots,s)
        res.append([s,round(abs((b-a)*100/a),3)])
        res.sort(key=lambda x:-x[1])
    df = pd.DataFrame(res)
    df = pd.DataFrame(res,columns=["Survey No","area-diff-percent"])
    df.to_csv('area-diff-percent.csv')

def t_intersects_s(tippan_fixed,survey_plots,s):
    sql = f'''select st_area(st_intersection(p.geom,q.geom))
            from {tippan_fixed} as p, {survey_plots} as q
            where p.kide = '{s}' and q.survey_no = '{s}' '''
    curr.execute(sql)
    a = curr.fetchall()[0][0]

    if (a == None or not a):
        return 0
    else:
        return float(a)
def t_union_s(tippan_fixed,survey_plots,s):
    sql = f'''select st_area(st_union(p.geom,q.geom))
            from {tippan_fixed} as p, {survey_plots} as q
            where p.kide = '{s}' and q.survey_no = '{s}' '''
    curr.execute(sql)
    # print(curr.fetchall()[0][0])
    # print("done")
    a = curr.fetchall()[0][0]
    if (a == None or not a):
        return 0
    else:
        return float(a)
def get_geom(tippan_fixed,s):
    sql = f'''select geom from {tippan_fixed} where kide = '{s}' '''
    curr.execute(sql)
    a = curr.fetchall()[0][0]
    return a

def get_a1_geom(tippan_fixed,survey_plots,s):
    sql = f'''select (st_multi(st_difference(p.geom,st_force2D(q.geom))))
            from {tippan_fixed} as p, {survey_plots} as q
            where p.kide = '{s}' and 
            q.survey_no = '{s}' '''
    curr.execute(sql)
    a = curr.fetchall()[0][0]
   
    return a

def get_a2_geom(tippan_fixed,survey_plots,s):
    sql = f'''select (st_multi(st_difference(st_force2D(q.geom),p.geom)))
            from {tippan_fixed} as p, {survey_plots} as q
            where p.kide = '{s}' and 
            q.survey_no = '{s}' '''
    curr.execute(sql)
    a = curr.fetchall()[0][0]
    return a



def create_result_table(result_table):
    sql = f''' drop table if exists {result_table};
                create table {result_table}(
                    survey_no varchar,
                    area_tippan float,
                    area_survey float,
                    a1 geometry(MultiPolygon,32643),
                    a1_area float,
                    a2 geometry(MultiPolygon,32643),
                    a2_area float,
                    geom geometry(MultiPolygon,32643));'''
    curr.execute(sql)
    sur_no = find_survey_no(tippan_trans,survey_plots)
    data = []
    sum_a1 = 0
    sum_a2 = 0
    for s in sur_no:
        area_t = area_of(tippan_fixed,s)
        area_s = area_of_survey(survey_plots,s)
        a1 = get_a1_geom(tippan_fixed,survey_plots,s)
        a1_area = area_t - t_intersects_s(tippan_fixed,survey_plots,s)
        a2 = get_a2_geom(tippan_fixed,survey_plots,s)
        a2_area = t_union_s(tippan_fixed,survey_plots,s) - area_t
        geom = get_geom(tippan_fixed,s)
        # data.append([s,area_t,area_s,a1,a1_area])
        sum_a1  = sum_a1 + a1_area
        sum_a2 = sum_a2 + a2_area
        data.append([s,area_t,area_s,a1,a1_area,a2,a2_area,geom])
    print("a1 before : ",sum_a1)
    print("a2 before : ",sum_a2)
    execute_batch(curr,f"insert into {result_table} values(%s,%s,%s,%s,%s,%s,%s,%s)", data)

def create_a1_table():
    alpha = 5  
    sql = f'''drop table if exists a1_temp_data;
    CREATE TABLE a1_temp_data AS 
    SELECT survey_no,(ST_DUMP(a1)).geom::geometry(Polygon,32643) AS geom FROM {result_table};

    alter table a1_temp_data 
    add column intersection_area float;

    update a1_temp_data
    set intersection_area = (select max(st_area(st_intersection(a1_temp_data.geom,q.geom)))
    from {tippan_fixed} as q where
    a1_temp_data.survey_no != q.kide);

    drop table if exists a1;
    create table a1 as select * from a1_temp_data;

    delete from a1_temp_data 
    where intersection_area < 0.01*st_area(geom);

    
    delete from a1_temp_data 
    where st_area(geom)/st_perimeter(geom) < {alpha};
    
   
    drop table if exists a1_temp_data2;
    create table a1_temp_data2 as 
    select survey_no,st_union(geom) as geom from a1_temp_data group by survey_no;
    
    drop table if exists {tippan_fixed2};
    create table {tippan_fixed2} as select * from {tippan_fixed} where 1 = 1;
    alter table {tippan_fixed2}
    rename excess_area to a1_area;
    alter table {tippan_fixed2}
    rename cross_over to a2_area; 
    '''    
    curr.execute(sql)
    sur_no = find_survey_no(tippan_trans,'a1_temp_data2')
    for s in sur_no:
        sql = f''' 
        update {tippan_fixed2} 
                set geom = st_multi(st_difference({tippan_fixed2}.geom,q.geom)) from
                     a1_temp_data2 as q 
                    where {tippan_fixed2}.kide = '{s}' and q.survey_no = '{s}'; 
                '''
        curr.execute(sql)
    
    sur_no = find_survey_no(tippan_trans,survey_plots)
    
    sum_a1 = 0
    sum_a2 = 0
    for s in sur_no:
        area_t = area_of(tippan_fixed2,s)
        a1_area = area_t - t_intersects_s(tippan_fixed2,survey_plots,s)
        a2_area = t_union_s(tippan_fixed2,survey_plots,s) - area_t
        sql = f''' 
        update {tippan_fixed2} 
               set a1_area = {a1_area}
                    where {tippan_fixed2}.kide = '{s}' ; 
        update {tippan_fixed2}
                set a2_area = {a2_area}
                where {tippan_fixed2}.kide = '{s}'
                '''
        curr.execute(sql)
        sum_a1  = sum_a1 + a1_area
        sum_a2 = sum_a2 + a2_area
    print("a1 after : ",sum_a1)
    print("a2 after : ",sum_a2)
    
def create_a2_table():
    sql = f'''drop table if exists a2;
            create table a2 as
            select st_difference(st_union(q.geom),st_union(p.geom))
            from {tippan_fixed} as p inner join {survey_plots} as q 
            on p.kide = q.survey_no;

        drop table if exists a2_temp_data;
        CREATE TABLE a2_temp_data AS 
        SELECT (ST_DUMP(a2)).geom::geometry(Polygon,32643) AS geom FROM {result_table};
'''
    curr.execute(sql)


n = 6
curr = connect_to_DB()

bnds = ((-10, 10), (-10, 10), (-6.2,6.2))

tippan_bad = 'barhanpur_tippan_bad'
tippan_trans = 'barhanpur_tippan_trans2'
survey_plots = 'barhanpur_survey_plots'
tippan_fixed = 'barhanpur_tippan_fixed62'
tippan_fixed2 = 'barhanpur_tippan_fixed6_cropped'
result_table = 'barhanpur_tippan_result_table'


def main():
    fix_tippans()
    create_result_table(result_table)
    create_a1_table()
    create_a2_table()

if __name__ == "__main__":
    main()