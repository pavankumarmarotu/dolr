import argparse
import logging
import os
import psycopg2
import geopandas as gpd
from queue import Queue
import time

# CHANGE 15m to 5m in BUFFER


class PGConn:
    def __init__(self, host, port, dbname, user=None, passwd=None):
        self.host = host
        self.port = port
        self.dbname = dbname
        if user is not None:
            self.user = user
        else:
            self.user = ""
        if passwd is not None:
            self.passwd = passwd
        else:
            self.passwd = ""
        self.conn = None

    def connection(self):
        """Return connection to PostgreSQL.  It does not need to be closed
        explicitly.  See the destructor definition below.

        """
        if self.conn is None:
            conn = psycopg2.connect(dbname=self.dbname,
                                    host=self.host,
                                    port=str(self.port),
                                    user=self.user,
                                    password=self.passwd)
            self.conn = conn
            
        return self.conn

    def __del__(self):
        """No need to explicitly close the connection.  It will be closed when
        the PGConn object is garbage collected by Python runtime.

        """
        print(self.conn)
        self.conn.close()
        self.conn = None


class SetupVectorial_Map:
    def __init__(self, pgconn, input_edges_table="barhanpur.edges_cleaned3", input_schema="barhanpur", input_farm_table="barhanpur.crop_plots_clipped",
        farm_topo_schema="farm_topo2", cadastral_topo_schema="cadastral_topo1"):

        self.input_edges_table=input_edges_table
        self.input_schema=input_schema
        self.input_farm_table=input_farm_table
        self.farm_topo_schema=farm_topo_schema
        self.cadastral_topo_schema=cadastral_topo_schema
    

    def check_schema_exists(self, pgconn, schema):
        sql_query="""
            select schema_name from information_schema.schemata where schema_name='{schema}'
        """.format(schema=schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            schema_out=curs.fetchone()
        
        if schema_out is not None:
            return True
        else:
            return False
    

    def remove_one_deg_nodes(self, pgconn):
        sql_query="""
            drop table if exists {schema}.node_edge_collection;
            create table {schema}.node_edge_collection as

            select
                start_node as node
            from
                {farm_schema}.edge_data
            ;

            insert into {schema}.node_edge_collection
            select end_node as node
            from {farm_schema}.edge_data
            ;


            drop table if exists {schema}.removable_nodes;
            create table {schema}.removable_nodes as

            select
                node
            from
                {schema}.node_edge_collection
            where
                node in (
                    select
                        node
                    from
                        {schema}.node_edge_collection
                    group by
                        node
                    having
                        (count(*) <= 1)
                )
            ;


            drop table if exists {schema}.removable_edges;
            create table {schema}.removable_edges as

            with single_nodes as (
                select node from {schema}.removable_nodes
            )

            select
                edge_id
            from
                {farm_schema}.edge_data edges, single_nodes
            where
                edges.start_node=single_nodes.node
                or edges.end_node=single_nodes.node
            ;

            with removable_edges as (
                select edge_id from {schema}.removable_edges
            )
            select st_remedgemodface('{farm_schema}', edge_id) from removable_edges;

            with removable_nodes as (
                select node from {schema}.removable_nodes
            )
            select st_removeisonode('{farm_schema}', node) from removable_nodes;
        """.format(schema=self.input_schema, farm_schema=self.farm_topo_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()


    def create_farm_topology(self, pgconn):

        # farm_topo1 is a temporary farm schema
        if self.check_schema_exists(pgconn, "farm_topo1"):
            comment_farm_topo_temp_drop=""
        else:
            comment_farm_topo_temp_drop="--"
                
        if self.check_schema_exists(pgconn, self.farm_topo_schema):
            comment_farm_topo_drop=""
        else:
            comment_farm_topo_drop="--"
                
        input_fname=os.path.join(os.path.dirname(__file__), "general_farm_voronoi.sql")
        with open(input_fname, "r") as in_file:
            sql_query=in_file.read().format(
                schema=self.input_schema,
                farm_schema=self.farm_topo_schema,
                input_farm_table=self.input_farm_table,
                comment_farm_topo_temp_drop=comment_farm_topo_temp_drop,
                comment_farm_topo_drop=comment_farm_topo_drop
            )
        
        output_fname=os.path.join(os.path.dirname(__file__), "farm_voronoi_output.sql")
        with open(output_fname, "w") as out_file:
            out_file.write(sql_query)
        
        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()

        for _ in range(10):
            self.remove_one_deg_nodes(pgconn)
    
    def create_cadastral_topology(self, pgconn):
        if self.check_schema_exists(pgconn, self.cadastral_topo_schema):
            comment_cadastral_topo_drop=""
        else:
            comment_cadastral_topo_drop="--"
        
        sql_query="""
        {comment_cadastral_topo_drop}select DropTopology('{cadastral_topo_schema}');
        select CreateTopology('{cadastral_topo_schema}', 32643);

        select st_createtopogeo('{cadastral_topo_schema}', geom)
        from (
            select
                st_simplifypreservetopology(st_collect(st_force2d(geom)), 10) as geom
            from
                {edges_cleaned}
        ) as foo;

        with edges as (
            select edge_id, start_node, end_node, geom from {cadastral_topo_schema}.edge_data
        ),
        boundary as (
            select
                (st_dumppoints(st_segmentize(geom, 10000))).geom as point
            from
                edges
        )

        select topogeo_addpoint('{cadastral_topo_schema}', point, 1) from boundary;
        """.format(cadastral_topo_schema=self.cadastral_topo_schema, edges_cleaned=self.input_edges_table,
            comment_cadastral_topo_drop=comment_cadastral_topo_drop)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    

    def clean_nodes(self, pgconn):
        sql_query = """
        with two_points as (
            with neigh as (
                select
                    count(edge_id),
                    node_id
                from
                    {cadastral_topo_schema}.edge as p,
                    {cadastral_topo_schema}.node
                where
                    start_node = node_id
                    or end_node = node_id
                group by
                    node_id
            )
            select
                r.node_id, r.geom
            from
                {cadastral_topo_schema}.node as r,
                neigh
            where
                r.node_id = neigh.node_id
                and (neigh.count = 2)
        )
        select p.edge_id as e1, q.edge_id as e2 from {cadastral_topo_schema}.edge as p, {cadastral_topo_schema}.edge as q
        where
            p.edge_id != q.edge_id and
            st_intersection(p.geom,q.geom) in (select geom from two_points) and
            (
                (degrees(st_angle(p.geom,q.geom)) > 170
                and degrees(st_angle(p.geom,q.geom)) < 190) or
                degrees(st_angle(p.geom,q.geom)) < 10
                or degrees(st_angle(p.geom,q.geom)) > 350
            )
        ;
        """.format(cadastral_topo_schema=self.cadastral_topo_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            all_pairs=curs.fetchall()
        
            visited=[]
            for pairs in all_pairs:
                if pairs[0] in visited or pairs[1] in visited:
                    continue
                sql_query="""select st_modedgeheal('{cadastral_topo_schema}', {pair1}, {pair2})""".format(
                    cadastral_topo_schema=self.cadastral_topo_schema, pair1=pairs[0], pair2=pairs[1]
                )
                visited.append(pairs[0])
                visited.append(pairs[1])
                curs.execute(sql_query)
        pgconn.commit()
    

    def add_srid_32643(self, pgconn):
        sql_query="""
            alter table sawangi.crop_plots
            add column geom_32643 geometry(Geometry, 32643);

            update sawangi.crop_plots cp
            set geom_32643 = st_transform(cp_dup.geom, 32643)
            from sawangi.crop_plots cp_dup
            where cp.gid=cp_dup.gid;

            drop table if exists sawangi.crop_plots_clipped;
            create table sawangi.crop_plots_clipped as 

            with buffered_cadastrals as (
                select st_buffer(st_union(geom), 15) as geom from sawangi.cadastrals_cleaned
            )
            select
                cp.*
            from
                sawangi.crop_plots cp,
                buffered_cadastrals buf
            where
                st_intersects(cp.geom_32643, buf.geom)
            ;

            create index sawangi_clipped_geom_32643_idx
            on sawangi.crop_plots_clipped using gist(geom_32643);

        """

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    

    def make_faces(self, pgconn):
        sql_query="""
            select polygonize('{cadastral_topo_schema}');

            drop table if exists {schema}.original_faces;
            create table {schema}.original_faces as
            
            select
                face_id,
                st_makevalid(st_getfacegeometry('{cadastral_topo_schema}', face_id)) as geom
            from
                {cadastral_topo_schema}.face
            where
                face_id>0
            ;
        """.format(schema=self.input_schema, cadastral_topo_schema=self.cadastral_topo_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()


# Barhanpur: generally takes around 5-6 minutes to run for the Voronoi for farm plots. Cadastral code takes about 30 seconds.
if __name__=="__main__":

    start_timer=time.time()

    print("------ESTABLISHING CONNECTION------")
    ###### EDIT THIS ######
    pgconn_obj = PGConn(
        "localhost",
        5432,
        "pocra",
        "postgresql_user",
        "postgresql_password")
    
    pgconn=pgconn_obj.connection()
    
    cur_village="sawangi"

    if cur_village=="barhanpur":
        vec=SetupVectorial_Map(pgconn, input_edges_table="barhanpur.edges_cleaned3", input_schema="barhanpur",
            input_farm_table="barhanpur.crop_plots_clipped", farm_topo_schema="barhanpur_farm_topo", cadastral_topo_schema="barhanpur_cadastral_topo")
    elif cur_village=="sawangi":
        vec=SetupVectorial_Map(pgconn, input_edges_table="sawangi.edges_cleaned2", input_schema="sawangi",
            input_farm_table="sawangi.temp", farm_topo_schema="sawangi_farm_topo", cadastral_topo_schema="sawangi_cadastral_topo")
    elif cur_village=="mangrul":
        vec=SetupVectorial_Map(pgconn, input_edges_table="anthrokrishi.edges_cleaned1", input_schema="anthrokrishi",
            input_farm_table="anthrokrishi.mangrul_clipped_2019_10_17", farm_topo_schema="mangrul_farm_topo", cadastral_topo_schema="mangrul_cadastral_topo")
    
    print("------CREATING FARM TOPOLOGY------")
    vec.create_farm_topology(pgconn)
    time.sleep(1)

    print("------CREATING TOPOLOGY------")
    vec.create_cadastral_topology(pgconn)
    print("------CLEANING NODES------")
    for _ in range(6):
        vec.clean_nodes(pgconn)

    print("------MAKING FACES------")
    vec.make_faces(pgconn)
    time.sleep(1)

    end_timer=time.time()
    print("Time taken:", end_timer-start_timer)
