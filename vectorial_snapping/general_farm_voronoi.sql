drop table if exists {schema}.crop_plots_clipped_dedup;
create table {schema}.crop_plots_clipped_dedup as

select gid, geom_32643 from {input_farm_table};


with cp as (
	select gid, geom_32643 from {schema}.crop_plots_clipped_dedup
),
duplicate_geom as (
	select
		cp2.gid
	from
		cp cp1,
		cp cp2
	where
		st_intersects(cp1.geom_32643, cp2.geom_32643)
		and (st_area(st_intersection(cp1.geom_32643, cp2.geom_32643))/st_area(cp1.geom_32643)>0.5
			 or st_area(st_intersection(cp1.geom_32643, cp2.geom_32643))/st_area(cp2.geom_32643)>0.5)
		and cp1.gid!=cp2.gid
		and (st_area(cp1.geom_32643)>st_area(cp2.geom_32643)
			 or (st_area(cp1.geom_32643)=st_area(cp2.geom_32643) and cp1.gid>cp2.gid))
	order by
		gid
)

delete from {schema}.crop_plots_clipped_dedup dedup
using duplicate_geom
where dedup.gid=duplicate_geom.gid
;


drop table if exists {schema}.farm_boundary;
create table {schema}.farm_boundary as

select
	(st_dumppoints(st_segmentize(st_linemerge(st_boundary(geom_32643)), 4))).geom as geom,
	gid
from
	{schema}.crop_plots_clipped_dedup
;

insert into {schema}.farm_boundary
select
	st_translate((st_dumppoints(st_segmentize(st_linemerge(st_boundary(geom_32643)), 3))).geom,
		0.005, 0.005) as geom,
	gid
from
	{schema}.crop_plots_clipped_dedup
;


drop table if exists {schema}.farm_voronoi_space_partition;
create table {schema}.farm_voronoi_space_partition as

select
	(st_dump(st_voronoipolygons(st_collect(geom)))).geom as polygons
from
	{schema}.farm_boundary
;


drop table if exists {schema}.farm_voronoi_blobs;
create table {schema}.farm_voronoi_blobs as

with voronoi as (
	select * from {schema}.farm_voronoi_space_partition
),
voronoi_intersection_ratios as (
	select
		voronoi.polygons as polygons,
		cp.gid as gid,
		st_area(st_intersection(voronoi.polygons, cp.geom_32643))/st_area(voronoi.polygons) as intersection_ratio
	from
		voronoi, {schema}.crop_plots_clipped_dedup cp
	where
		st_intersects(voronoi.polygons, cp.geom_32643)
),
voronoi_max_ratios as (
	select
		polygons,
		max(intersection_ratio) as max_int_ratio
	from
		voronoi_intersection_ratios
	group by
		polygons
)

select
	voronoi_intersection_ratios.gid,
	st_union(voronoi_max_ratios.polygons) as voronoi
from
	voronoi_intersection_ratios
inner join
	voronoi_max_ratios
on
	voronoi_intersection_ratios.polygons=voronoi_max_ratios.polygons
	and voronoi_intersection_ratios.intersection_ratio=voronoi_max_ratios.max_int_ratio
group by
	voronoi_intersection_ratios.gid
;


drop table if exists {schema}.farm_multi_voids;
create table {schema}.farm_multi_voids as

with voronoi_plots as (
	select gid, voronoi as geom from {schema}.farm_voronoi_blobs
),
cp as (
	select gid, geom_32643 as geom from {schema}.crop_plots_clipped_dedup
),
buffered_cp as (
	select cp.gid, st_buffer(cp.geom, 3) as buffer from cp
),
voids_multi as (
	select
		buffered_cp.gid as gid,
		(case when
		 	st_area(st_difference(voronoi_plots.geom, buffered_cp.buffer))>0.03*st_area(buffered_cp.buffer)
		 then
		 	st_difference(voronoi_plots.geom, buffered_cp.buffer)
		 else
		 	st_geomfromtext('POLYGON EMPTY', 32643)
		 end) as geom
	from
		buffered_cp
	inner join
		voronoi_plots
	on
		voronoi_plots.gid=buffered_cp.gid
)

select * from voids_multi;


drop table if exists {schema}.farm_voids;
create table {schema}.farm_voids as

with voronoi_plots as (
	select gid, voronoi as geom from {schema}.farm_voronoi_blobs
),
cp as (
	select gid, geom_32643 as geom from {schema}.crop_plots_clipped_dedup
),
buffered_cp as (
	select gid, st_buffer(cp.geom, 3) as buffer from cp
),
voids_multi as (
	select * from {schema}.farm_multi_voids
),
voids as (
	select
		voronoi_plots.gid as gid,
		(st_dump(voids_multi.geom)).geom as geom
	from
		voronoi_plots
	inner join
		voids_multi
	on
		voronoi_plots.gid=voids_multi.gid
)

select * from voids;


insert into {schema}.farm_voids
select * from {schema}.farm_multi_voids
where st_astext({schema}.farm_multi_voids.geom)='POLYGON EMPTY';


drop table if exists {schema}.farm_voronoi_with_voids;
create table {schema}.farm_voronoi_with_voids as

with voronoi_plots as (
	select gid, voronoi as geom from {schema}.farm_voronoi_blobs
),
cp as (
	select gid, geom_32643 as geom from {schema}.crop_plots_clipped_dedup
),
voids as (
	select gid, geom from {schema}.farm_voids
),
voids_qual as (
	select
		voids.gid,
		(case when st_area(voids.geom)>0.03*st_area(cp.geom)
		 then voids.geom
		 else st_geomfromtext('POLYGON EMPTY', 32643)
		 end) as geom
	from
		voids
	inner join
		cp
	on
		voids.gid=cp.gid
),
final_voids as (
	select
		gid,
		st_union(geom) as geom
	from
		voids_qual
	group by
		gid
)

select
	voronoi_plots.gid as gid,
	st_difference(voronoi_plots.geom, final_voids.geom) as geom
from
	voronoi_plots
inner join
	final_voids
on
	voronoi_plots.gid=final_voids.gid
;


drop table if exists {schema}.diff_blobs;
create table {schema}.diff_blobs as

with voronoi_blobs as (
	select gid, geom from {schema}.farm_voronoi_with_voids
),
diff_blobs as (
	select
		va.gid as gid,
		st_union(vb.geom) as geom
	from
		voronoi_blobs va,
		voronoi_blobs vb
	where
		va.gid>vb.gid
		and st_area(st_intersection(va.geom, vb.geom))>0
	group by
		va.gid, va.geom
)

select gid, geom from diff_blobs;


with voronoi_blobs as (
	select gid, geom from {schema}.farm_voronoi_with_voids
),
leftover_gids as (
	select
		gid
	from
		voronoi_blobs
	where
		not exists (
			select diff.gid from {schema}.diff_blobs diff where diff.gid=voronoi_blobs.gid
		)
)

insert into {schema}.diff_blobs
select gid, st_geomfromtext('POLYGON EMPTY', 32643) as geom
from leftover_gids
;


drop table if exists {schema}.farm_voronoi_clipped;
create table {schema}.farm_voronoi_clipped as

with voronoi_blobs as (
	select gid, geom from {schema}.farm_voronoi_with_voids
),
diff as (
	select gid, geom from {schema}.diff_blobs
)

select
	v.gid, st_geometryN(st_collectionextract(st_difference(v.geom, diff.geom)), 1) as geom
from
	voronoi_blobs v,
	diff
where
	v.gid=diff.gid
;

-- Skip formation of topo1 (or maybe not)

{comment_farm_topo_temp_drop}select DropTopology('farm_topo1');
select CreateTopology('farm_topo1', 32643);

select st_createtopogeo('farm_topo1', st_collect(geom))
from {schema}.farm_voronoi_clipped;

{comment_farm_topo_drop}select DropTopology('{farm_schema}');
select CreateTopology('{farm_schema}', 32643);

select st_createtopogeo('{farm_schema}', geom)
from (
	select
		st_collect(st_simplifypreservetopology(geom, 5)) as geom
	from
		farm_topo1.edge_data
) as foo;


with edges as (
	select edge_id, start_node, end_node, geom from {farm_schema}.edge_data
),
boundary as (
	select
		(st_dumppoints(st_segmentize(geom, 2000))).geom as point
	from
		edges
)

select topogeo_addpoint('{farm_schema}', point, 1) from boundary;

-- Takes 3 minutes 15 seconds --
