import argparse
import logging
import os
import psycopg2
import geopandas as gpd
from queue import Queue, PriorityQueue
import time

# CHANGE len() > 0 to len() > -1 to see jump in rating at the end. Doesn't matter either ways, for rating, improves performance.

class PGConn:
    def __init__(self, host, port, dbname, user=None, passwd=None):
        self.host = host
        self.port = port
        self.dbname = dbname
        if user is not None:
            self.user = user
        else:
            self.user = ""
        if passwd is not None:
            self.passwd = passwd
        else:
            self.passwd = ""
        self.conn = None

    def connection(self):
        """Return connection to PostgreSQL.  It does not need to be closed
        explicitly.  See the destructor definition below.

        """
        if self.conn is None:
            conn = psycopg2.connect(dbname=self.dbname,
                                    host=self.host,
                                    port=str(self.port),
                                    user=self.user,
                                    password=self.passwd)
            self.conn = conn
            
        return self.conn

    def __del__(self):
        """No need to explicitly close the connection.  It will be closed when
        the PGConn object is garbage collected by Python runtime.

        """
        print(self.conn)
        self.conn.close()
        self.conn = None


class Vectorial_Map:
    def __init__(self, pgconn, input_schema="barhanpur", farm_topo_schema="farm_topo2",
        cadastral_topo_schema="cadastral_topo1", shifted_topo_schema="cadastral_shifted_topo1",
        experimental_topo_schema="sawangi_experimental_topo", num_node_ids=20000):

        self.input_schema=input_schema
        self.farm_topo_schema=farm_topo_schema
        self.cadastral_topo_schema=cadastral_topo_schema
        self.shifted_topo_schema=shifted_topo_schema
        self.experimental_topo_schema=experimental_topo_schema
        self.num_node_ids=num_node_ids

        self.visited_nodes={}
        self.visited_nodes_2={}
        self.farm_bfs_visited_nodes_cadastral={}
        self.farm_bfs_visited_nodes_farm={}

        self.fvisited_nodes={}
        self.fvisited_faces={}
        self.fvisited_faces_num={}
        self.fvisited_neighbour_num={}
        self.nodes_to_faces={}
        self.faces_to_nodes={}
        self.face_traversal_order={}
        self.shifted_face_rating={}
        self.node_order={}
        self.num_nodes_covered=1

        self.rating_threshold=0.95
        self.area_diff_threshold=5
        self.shape_diff_threshold=5
        
        for node_id in range(num_node_ids):
            self.visited_nodes[node_id]=False
            self.visited_nodes_2[node_id]=False
            self.farm_bfs_visited_nodes_cadastral[node_id]=False
            self.farm_bfs_visited_nodes_farm[node_id]=False

            self.fvisited_nodes[node_id]=False
            self.fvisited_faces[node_id]=False
            self.fvisited_faces_num[node_id]=0
            self.nodes_to_faces[node_id]=[]
            self.faces_to_nodes[node_id]=[]
            self.fvisited_neighbour_num[node_id]={}

        sql_query="""
            drop table if exists {schema}.shifted_nodes;
            create table {schema}.shifted_nodes (
                node_id integer,
                geom geometry(Point, 32643)
            );
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()

        sql_query="""
            drop table if exists {schema}.snapped_nodes;
            create table {schema}.snapped_nodes (
                node_id integer,
                geom geometry(Point, 32643)
            );
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    

    def check_schema_exists(self, pgconn, schema):
        sql_query="""
            select schema_name from information_schema.schemata where schema_name='{schema}'
        """.format(schema=schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            schema_out=curs.fetchone()
        
        if schema_out is not None:
            return True
        else:
            return False
    

    def get_num_original_faces(self, pgconn):
        sql_query="""
            select face_id from {schema}.original_faces;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            num_faces=len(curs.fetchall())
        
        return num_faces
    

    def get_edge_length(self, pgconn, node_id_1, node_id_2):
        sql_query="""
            select
                st_length(geom)
            from
                {cadastral_topo_schema}.edge
            where
                (start_node={node_id_1} and end_node={node_id_2})
                or
                (start_node={node_id_2} and end_node={node_id_1})
            ;
        """.format(cadastral_topo_schema=self.cadastral_topo_schema,
                   node_id_1=node_id_1, node_id_2=node_id_2)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            edge_length=curs.fetchone()
        
        if edge_length is not None:
            edge_length=edge_length[0]
        else:
            edge_length=0
        
        return edge_length
    

    def insert_shifted_node(self, node_id, shifted_geom):
        sql_query="""
            insert into {schema}.shifted_nodes
            values ({node_id}, st_geomfromtext('{shifted_geom}', 32643));
        """.format(schema=self.input_schema, node_id=node_id, shifted_geom=shifted_geom)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()

        self.node_order[node_id]=self.num_nodes_covered
        self.num_nodes_covered+=1
    
    
    def get_neighbours(self, pgconn, node_id):
        edge_endpoints=[]
        with pgconn.cursor() as curs:
            sql_query="""
                select end_node from {cadastral_topo_schema}.edge where start_node={node_id};
            """.format(cadastral_topo_schema=self.cadastral_topo_schema, node_id=node_id)

            curs.execute(sql_query)
            end_nodes=curs.fetchall()
            for node in end_nodes:
                edge_endpoints.append(node[0])
            
            sql_query="""
                select start_node from {cadastral_topo_schema}.edge where end_node={node_id};
            """.format(cadastral_topo_schema=self.cadastral_topo_schema, node_id=node_id)
            
            curs.execute(sql_query)
            start_nodes=curs.fetchall()
            for node in start_nodes:
                edge_endpoints.append(node[0])
                
        return edge_endpoints

    
    def check_node_shifted(self, pgconn, node_id):
        sql_query="""
            select * from {schema}.shifted_nodes where node_id={node_id};
        """.format(schema=self.input_schema, node_id=node_id)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            shifted_node=curs.fetchone()
        
        if shifted_node is not None:
            return True
        else:
            return False
    

    def check_geom_in_shifted_nodes(self, pgconn, geom):
        sql_query="""
            select node_id from {schema}.shifted_nodes where geom='{geom}'::geometry;
        """.format(schema=self.input_schema, geom=geom)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            shifted_node=curs.fetchone()

        if shifted_node is not None:
            return True
        else:
            return False
    
    
    def get_node_shift(self, pgconn, node_id):
        sql_query="""
            with original_node as (
                select
                    node_id,
                    geom
                from
                    {cadastral_topo_schema}.node
                where
                    node_id={node_id}
            ),
            shifted_node as (
                select
                    node_id,
                    geom
                from
                    {schema}.shifted_nodes
                where
                    node_id={node_id}
            )

            select
                st_x(shifted_node.geom)-st_x(original_node.geom) as x_delta,
                st_y(shifted_node.geom)-st_y(original_node.geom) as y_delta
            from
                original_node, shifted_node
            where
                original_node.node_id={node_id} and
                shifted_node.node_id={node_id}
            ;
        """.format(cadastral_topo_schema=self.cadastral_topo_schema, schema=self.input_schema, node_id=node_id)

        x_delta=0
        y_delta=0
        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            pos=curs.fetchone()
            if pos is not None:
                (x_delta, y_delta)=pos
        
        return x_delta, y_delta
    

    def unshift_node(self, pgconn, node_id):
        sql_query="""
            delete from {schema}.shifted_nodes
            where node_id={node_id}
            ;

            delete from {schema}.snapped_nodes
            where node_id={node_id}
            ;
        """.format(schema=self.input_schema, node_id=node_id)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    
    
    def translate_node(self, pgconn, node_id_1, node_id_2):
        x_delta, y_delta=self.get_node_shift(pgconn, node_id_1)

        sql_query="""
            with farm_voronoi as (
                select
                    st_collect(geom) as geom
                from
                    {farm_topo_schema}.node
            ),
            point_coordinates as (
                select
                    node_id,
                    st_x(geom) as x,
                    st_y(geom) as y
                from
                    {cadastral_topo_schema}.node
                where
                    node_id={node_id_2}
            ),
            point as (
                select
                    node_id,
                    st_point(x, y, 32643) as geom
                from
                    point_coordinates
            ),
            shifted_point as (
                select
                    node_id,
                    st_translate(geom, {x_delta}, {y_delta}) as geom
                from
                    point
            )

            insert into {schema}.shifted_nodes
            select
                node_id,
                geom
            from
                shifted_point
            ;
        """.format(schema=self.input_schema, farm_topo_schema=self.farm_topo_schema, cadastral_topo_schema=self.cadastral_topo_schema,
            node_id_2=node_id_2, x_delta=x_delta, y_delta=y_delta)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    
    
    # Takes node 1, duplicates and translates it by the vector of the original cadastral, snaps it to the closest farm plot
    # point, adds it to the modified nodes table
    def snap_node(self, pgconn, node_id_1, node_id_2):
        x_delta, y_delta=self.get_node_shift(pgconn, node_id_1)
        edge_length=self.get_edge_length(pgconn, node_id_1, node_id_2)
        
        sql_query="""
            with farm_voronoi as (
                select
                    st_collect(geom) as geom
                from
                    {farm_topo_schema}.node
            ),
            point_coordinates as (
                select
                    node_id,
                    st_x(geom) as x,
                    st_y(geom) as y
                from
                    {cadastral_topo_schema}.node
                where
                    node_id={node_id_2}
            ),
            point as (
                select
                    node_id,
                    st_point(x, y, 32643) as geom
                from
                    point_coordinates
            ),
            shifted_point as (
                select
                    node_id,
                    st_translate(geom, {x_delta}, {y_delta}) as geom
                from
                    point
            ),
            closest_point as (
                select
                    node_id,
                    shifted_point.geom as original_shifted_geom,
                    st_closestpoint(farm_voronoi.geom, shifted_point.geom) as farm_shifted_geom
                from
                    shifted_point, farm_voronoi
            )

            select
                node_id,
                farm_shifted_geom as geom
            from
                closest_point
            where
                st_distance(original_shifted_geom, farm_shifted_geom)<{edge_length}/10
            ;
        """.format(farm_topo_schema=self.farm_topo_schema, cadastral_topo_schema=self.cadastral_topo_schema,
                   node_id_2=node_id_2, x_delta=x_delta, y_delta=y_delta, edge_length=edge_length)
        
        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            final_tuple=curs.fetchone()
        
        if final_tuple is not None:
            (final_node_id, final_geom)=final_tuple
            geom_exists=self.check_geom_in_shifted_nodes(pgconn, final_geom)
            if not geom_exists:
                sql_query="""
                    insert into {schema}.shifted_nodes
                    values ({final_node_id}, '{final_geom}'::geometry)
                    ;
                """.format(schema=self.input_schema, final_node_id=final_node_id, final_geom=final_geom)

                with pgconn.cursor() as curs:
                    curs.execute(sql_query)                
                
                sql_query="""
                    insert into {schema}.snapped_nodes
                    values ({final_node_id}, '{final_geom}'::geometry)
                    ;
                """.format(schema=self.input_schema, final_node_id=final_node_id, final_geom=final_geom)

                with pgconn.cursor() as curs:
                    curs.execute(sql_query)
        pgconn.commit()

        node_shifted=self.check_node_shifted(pgconn, node_id_2)
        return node_shifted
    
    
    def show_shifted_nodes(self, pgconn):
        sql_query="""select node_id, geom from {schema}.shifted_nodes;""".format(schema=self.input_schema)
        shifted_nodes=[]
        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            shifted_nodes=curs.fetchall()
        for tuple in shifted_nodes:
            print(tuple[0], "\t", tuple[1])
    

    def copy_shifted_to_snapped(self, pgconn):
        sql_query="""
            drop table if exists {schema}.snapped_nodes;
            create table {schema}.snapped_nodes as

            select
                node_id,
                geom
            from
                {schema}.shifted_nodes
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    
    
    def first_bfs(self, pgconn, start_nodes):
        bfs_queue=Queue()
        for start_node_id in start_nodes:
            self.visited_nodes[start_node_id]=True
            bfs_queue.put(start_node_id)
        while not bfs_queue.empty():
            cur_node_id=bfs_queue.get()
            node_neighbours=self.get_neighbours(pgconn, cur_node_id)
            print(cur_node_id, node_neighbours)
            for node in node_neighbours:
                if not self.visited_nodes[node]:
                    node_snapped=self.snap_node(pgconn, cur_node_id, node)
                    if node_snapped:
                        self.visited_nodes[node]=True
                        bfs_queue.put(node)
        
    
    def second_bfs(self, pgconn, start_nodes):
        bfs_queue=Queue()
        for start_node_id in start_nodes:
            self.visited_nodes_2[start_node_id]=True
            bfs_queue.put(start_node_id)
        while not bfs_queue.empty():
            cur_node_id=bfs_queue.get()
            node_neighbours=self.get_neighbours(pgconn, cur_node_id)
            for node in node_neighbours:
                if not self.visited_nodes_2[node]:
                    node_shifted=self.check_node_shifted(pgconn, node)
                    if not node_shifted:
                        node_snapped=self.snap_node(pgconn, cur_node_id, node)
                        print("Node "+str(node)+" snapped in second BFS:", node_snapped)
                        if not node_snapped:
                            self.translate_node(pgconn, cur_node_id, node)
                    self.visited_nodes_2[node]=True
                    bfs_queue.put(node)
    
    
    def make_edges(self, pgconn):
        sql_query="""
        drop table if exists {schema}.shifted_edges;
        create table {schema}.shifted_edges as

        with original_edges as (
            select
                start_node,
                end_node
            from
                {cadastral_topo_schema}.edge
        ),
        shifted_nodes as (
            select
                node_id,
                geom
            from
                {schema}.shifted_nodes
        )

        select
            cadastral1.node_id as start_node,
            cadastral2.node_id as end_node,
            st_makeline(cadastral1.geom, cadastral2.geom) as geom
        from
            shifted_nodes cadastral1,
            shifted_nodes cadastral2,
            original_edges
        where
            original_edges.start_node=cadastral1.node_id and
            original_edges.end_node=cadastral2.node_id
        ;
        """.format(cadastral_topo_schema=self.cadastral_topo_schema, schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    
    
    def make_topology(self, pgconn):
        if self.check_schema_exists(pgconn, self.shifted_topo_schema):
            comment_shifted_topo_drop=""
        else:
            comment_shifted_topo_drop="--"
        
        sql_query="""
        {comment_shifted_topo_drop}select DropTopology('{shifted_topo_schema}');
        select CreateTopology('{shifted_topo_schema}', 32643);

        select st_createtopogeo('{shifted_topo_schema}', geom)
        from (
            select
                st_simplifypreservetopology(st_collect(geom), 5) as geom
            from
                {schema}.shifted_edges
        ) as foo
        ;
        """.format(shifted_topo_schema=self.shifted_topo_schema, schema=self.input_schema,
            comment_shifted_topo_drop=comment_shifted_topo_drop)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()

    
    # Creating a farm BFS  
    def create_farm_bfs(self, pgconn, start_nodes):
        bfs_queue=Queue()
        for start_node_id in start_nodes:
            self.farm_visited_nodes[start_node_id]=True
            bfs_queue.put(start_node_id)
        while not bfs_queue.empty():
            cur_node_id=bfs_queue.get()
            node_neighbours=self.get_neighbours(pgconn, cur_node_id)
            print(cur_node_id, node_neighbours)
            for node in node_neighbours:
                if not self.visited_nodes[node]:
                    self.snap_node(pgconn, cur_node_id, node)
                    self.visited_nodes[node]=True
                    bfs_queue.put(node)
    
    
    def get_farm_neighbours(self, pgconn, farm_node_id):
        edge_endpoints=[]
        with pgconn.cursor() as curs:
            sql_query="""
                select end_node from {farm_topo_schema}.edge where start_node={node_id};
            """.format(farm_topo_schema=self.farm_topo_schema, node_id=farm_node_id)

            curs.execute(sql_query)
            end_nodes=curs.fetchall()
            for node in end_nodes:
                edge_endpoints.append(node[0])
            
            sql_query="""
                select start_node from {farm_topo_schema}.edge where end_node={node_id};
            """.format(farm_topo_schema=self.farm_topo_schema, node_id=farm_node_id)
            
            curs.execute(sql_query)
            start_nodes=curs.fetchall()
            for node in start_nodes:
                edge_endpoints.append(node[0])
                
        return edge_endpoints
    
    
    def get_farm_neighbours_upto_10(self, pgconn, farm_node_id):
        temp_visited=[]
        bfs_queue=Queue()
        bfs_queue.put((farm_node_id, 0))
        while not bfs_queue.empty():
            cur_node_id_tuple=bfs_queue.get()
            node_neighbours=self.get_farm_neighbours(pgconn, cur_node_id_tuple[0])
            for next_node in node_neighbours:
                if next_node not in temp_visited and cur_node_id_tuple[1]<10:
                    temp_visited.append(next_node)
                    bfs_queue.put((next_node, cur_node_id_tuple[1]+1))
        return temp_visited
    

    def check_for_same_snap(self, pgconn):
        sql_query="""
            select
                n1.node_id as node_id_1,
                n2.node_id as node_id_2,
                n1.geom as common_geom
            from
                {schema}.shifted_nodes n1,
                {schema}.shifted_nodes n2
            where
                n1.geom=n2.geom and
                n1.node_id<n2.node_id
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            same_snaps=curs.fetchall()
        
        if same_snaps is None:
            print("All nodes have distinct positions, and hence snapped to different nodes")
        else:
            print("There are nodes which mapped to the same positions! They are:")
            print("node_id_1, node_id_2, common_geom")
            print(same_snaps)
    

    def make_face(self, pgconn, face_id):
        sql_query="""
            select (st_getfaceedges('{cadastral_topo_schema}', {face_id})).*;
        """.format(cadastral_topo_schema=self.cadastral_topo_schema, face_id=face_id)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            face_edges=curs.fetchall()
            edge_ids=[abs(edge[1]) for edge in face_edges]
        
        sql_query="""
            drop table if exists {schema}.temp_polygon_edges;
            create table {schema}.temp_polygon_edges (
                edge_id integer,
                geom geometry(LineString, 32643)
            );
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
        
        for edge_id in edge_ids:
            sql_query="""
                with edge as (
                    select
                        edge_id,
                        start_node,
                        end_node
                    from
                        {cadastral_topo_schema}.edge
                    where
                        edge_id={edge_id}
                ),
                nodes as (
                    select
                        node_id,
                        geom
                    from
                        {schema}.shifted_nodes
                )

                insert into {schema}.temp_polygon_edges
                select
                    edge_id,
                    st_makeline(nodes1.geom, nodes2.geom) as geom
                from
                    edge,
                    nodes nodes1,
                    nodes nodes2
                where
                    edge.start_node=nodes1.node_id
                    and edge.end_node=nodes2.node_id
                ;
            """.format(cadastral_topo_schema=self.cadastral_topo_schema,
                       schema=self.input_schema, edge_id=edge_id)
            
            with pgconn.cursor() as curs:
                curs.execute(sql_query)
            pgconn.commit()
        
        sql_query="""
            drop table if exists {schema}.temp_polygon;
            create table {schema}.temp_polygon as

            select
                (st_dump(st_polygonize(geom))).geom as geom
            from
                {schema}.temp_polygon_edges
            ;

            drop table if exists {schema}.mapped_polygon;
            create table {schema}.mapped_polygon as

            select
                original_faces.face_id as face_id,
                temp_polygon.geom as geom
            from
                {schema}.temp_polygon temp_polygon,
                {schema}.original_faces original_faces
            where
                st_intersects(temp_polygon.geom, original_faces.geom)
                and
                st_area(st_intersection(temp_polygon.geom, original_faces.geom))/
                    st_area(temp_polygon.geom) > 0.5
                and
                st_area(st_intersection(temp_polygon.geom, original_faces.geom))/
                    st_area(original_faces.geom) > 0.5
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    

    def check_face_valid(self, pgconn):

        sql_query="""
            select * from {schema}.mapped_polygon;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            polygons_created=curs.fetchall()
        
        if len(polygons_created)!=1:
            return False

        sql_query="""
            select
                st_isvalid(geom) as valid
            from
                {schema}.mapped_polygon
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            valid=curs.fetchone()
        
        if valid is None:
            valid=False
        else:
            valid=valid[0]
        
        return valid
    

    def get_shape_index(self, pgconn):
        sql_query="""
            select
                st_perimeter(geom) as perimeter,
                st_area(geom) as area
            from
                {schema}.mapped_polygon
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            (perimeter, area)=curs.fetchone()
        shape_index=perimeter**2/area

        return shape_index
    

    def get_area_diff(self, pgconn):
        sql_query="""
            select
                st_area(geom) as area
            from
                {schema}.mapped_polygon
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            shifted_area=curs.fetchone()[0]
        
        sql_query="""
            select
                st_area(original.geom) as area
            from
                {schema}.original_faces original,
                {schema}.mapped_polygon mapped
            where
                original.face_id=mapped.face_id
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            original_area=curs.fetchone()[0]
        
        area_diff=100*abs(1-shifted_area/original_area)
        return area_diff
    

    def evaluate_face(self, pgconn):
        sql_query="""
            with farm_plots as (
                select
                    gid as face_id,
                    geom_32643 as geom
                from
                    {schema}.crop_plots_clipped_dedup
            ),
            farm_buffer as (
                select
                    face_id,
                    st_buffer(geom, 20) as geom
                from
                    farm_plots
            ),
            farm_ownership as (
                select
                    farm_plots.face_id as farm_id,
                    st_area(st_intersection(farm_plots.geom, polygon.geom))/
                        st_area(farm_plots.geom) as intersection_ratio
                from
                    {schema}.mapped_polygon polygon,
                    farm_plots,
                    farm_buffer
                where
                    farm_plots.face_id=farm_buffer.face_id
                    and
                    st_intersects(polygon.geom, farm_buffer.geom)
                    and
                    not st_contains(polygon.geom, farm_buffer.geom)
            ),
            farm_rating as (
                select
                    farm_id,
                    case
                        when intersection_ratio>=0.5 then intersection_ratio
                        else 1-intersection_ratio
                    end as rating
                from
                    farm_ownership
            )

            select
                avg(rating) as rating
            from
                farm_rating
            ;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            rating=curs.fetchone()[0]
        
        if rating is None:
            rating=0

        return rating


    def setup_face_bfs(self, pgconn):
        sql_query="""
            select
                nodes.node_id,
                faces.face_id,
                nodes.geom as node_geom,
                faces.geom as face_geom
            from
                {cadastral_topo_schema}.node nodes,
                {schema}.original_faces faces
            where
                st_within(nodes.geom, st_buffer(faces.geom, 0.1))
            order by
                face_id
            ;
        """.format(schema=self.input_schema, cadastral_topo_schema=self.cadastral_topo_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            nodes_faces=curs.fetchall()
        pgconn.commit()
        
        for node_face_pairs in nodes_faces:
            node_id=node_face_pairs[0]
            face_id=node_face_pairs[1]
            self.nodes_to_faces[node_id].append(face_id)
            self.faces_to_nodes[face_id].append(node_id)

    
    def cover_face(self, pgconn, face_id, start_node):
        all_face_nodes=self.faces_to_nodes[face_id]
        visited_face_nodes={}
        for node in all_face_nodes:
            visited_face_nodes[node]=False
        
        node_queue=Queue()
        node_queue.put(start_node)
        visited_face_nodes[start_node]=True
        bordering_faces=set()
        nodes_shifted_list=[]

        while not node_queue.empty():
            cur_node_id=node_queue.get()
            node_neighbours=self.get_neighbours(pgconn, cur_node_id)
            print("(Face, Node):", face_id, cur_node_id)

            for node in node_neighbours:
                if face_id not in self.nodes_to_faces[node]:
                    continue
                if visited_face_nodes[node]:
                    continue

                node_shifted=self.check_node_shifted(pgconn, node)
                if not node_shifted:
                    node_snapped=self.snap_node(pgconn, cur_node_id, node)
                    if not node_snapped:
                        self.translate_node(pgconn, cur_node_id, node)
                    nodes_shifted_list.append(node)
                
                for node_face in self.nodes_to_faces[node]:
                    if node_face != face_id and not self.fvisited_faces[node_face]:
                        bordering_faces.add((node_face, node))
                visited_face_nodes[node]=True
                node_queue.put(node)
        
        faces_list=list(bordering_faces)
        self.fvisited_faces[face_id]=True

        self.make_face(pgconn, face_id)
        face_valid=self.check_face_valid(pgconn)
        print("Validity of face", face_id, ":", face_valid)

        if face_valid:
            rating=self.evaluate_face(pgconn)
            shape_index=self.get_shape_index(pgconn)
            area_diff=self.get_area_diff(pgconn)
            print("Area diff of face", face_id, ":", area_diff)
            print("Shape index of face", face_id, ":", shape_index)
        else:
            rating=0
        
        if (not face_valid or rating<self.rating_threshold or area_diff>self.area_diff_threshold) and len(nodes_shifted_list)>-1:
            self.fvisited_faces_num[face_id]+=1
            self.fvisited_faces[face_id]=False
            faces_list=[]
            print("Unshifted nodes for face {face_id} are:".format(face_id=face_id), nodes_shifted_list)
            for node_id in nodes_shifted_list:
                self.unshift_node(pgconn, node_id)
                if self.check_node_shifted(pgconn, node_id):
                    print("Node ID", node_id, "shouldn't be shifted. It is! Red alert, something's wrong.")
                    exit()
        else:
            self.shifted_face_rating[face_id]=rating
            for node in nodes_shifted_list:
                self.node_order[node]=self.num_nodes_covered
                self.num_nodes_covered+=1
        
        return faces_list, rating
    

    def face_bfs(self, pgconn, start_nodes):
        # Rating <- ascending (hence priority is 1-rating)
        face_queue=PriorityQueue()
        for node_id in start_nodes:
            for face_id in self.nodes_to_faces[node_id]:
                face_queue.put((0, face_id, node_id))
        num_faces_covered=1
        
        while not face_queue.empty():
            (rating, cur_face_id, cur_node_id)=face_queue.get()
            if self.fvisited_faces[cur_face_id]:
                continue
            if self.fvisited_faces_num[cur_face_id]>6:
                continue

            faces_list, rating=self.cover_face(pgconn, cur_face_id, cur_node_id)
            priority=round(1000*(1-rating))


            if self.fvisited_faces[cur_face_id]:
                self.face_traversal_order[cur_face_id]=num_faces_covered
                num_faces_covered+=1
                print("Face ID", cur_face_id, "is covered in position:", num_faces_covered)
                
                for pairs in faces_list:
                    neigh_face=pairs[0]
                    if cur_face_id not in self.fvisited_neighbour_num[neigh_face].keys():
                        self.fvisited_neighbour_num[neigh_face][cur_face_id]=0
                    self.fvisited_neighbour_num[neigh_face][cur_face_id]+=1

                    if self.fvisited_neighbour_num[neigh_face][cur_face_id]>2:
                        continue

                    face_queue.put((priority, pairs[0], pairs[1]))

            else:
                face_queue.put((priority, cur_face_id, cur_node_id))
    

    def fill_faces(self, pgconn, start_nodes):
        sql_query="""
            drop table if exists {schema}.rated_faces;
            create table {schema}.rated_faces as

            select * from {schema}.shifted_faces;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()

        self.rating_threshold=0
        self.area_diff_threshold=1000
        self.shape_diff_threshold=1000

        for node_id in range(self.num_node_ids):
            self.fvisited_nodes[node_id]=False
            self.fvisited_faces[node_id]=False
            self.fvisited_faces_num[node_id]=0
            self.fvisited_neighbour_num[node_id]={}
        
        self.face_bfs(pgconn, start_nodes)
    

    def extract_faces(self, pgconn):
        sql_query="""
            select polygonize('{shifted_topo_schema}');
            select polygonize('{farm_topo_schema}');
            select polygonize('{cadastral_topo_schema}');

            drop table if exists {schema}.shifted_edges_simplified;
            create table {schema}.shifted_edges_simplified as

            select
                geom
            from
                {shifted_topo_schema}.edge
            ;


            drop table if exists {schema}.shifted_faces;
            create table {schema}.shifted_faces (
                geom geometry(Polygon, 32643),
                face_id int generated by default as identity,
                face_order int default 999
            );

            insert into {schema}.shifted_faces
            select
                (st_dump(st_polygonize(geom))).geom as geom
            from
                {schema}.shifted_edges_simplified
            ;


            with original_faces as (
                select * from {schema}.original_faces
            ),
            shifted_faces as (
                select * from {schema}.shifted_faces
            ),
            face_matching as (
                select
                    shifted_faces.face_id as shifted_id,
                    original_faces.face_id as original_id
                from
                    shifted_faces,
                    original_faces
                where
                    st_intersects(shifted_faces.geom, original_faces.geom)
                    and
                    st_area(st_intersection(shifted_faces.geom, original_faces.geom))/
                        st_area(shifted_faces.geom) > 0.5
                    and
                    st_area(st_intersection(shifted_faces.geom, original_faces.geom))/
                        st_area(original_faces.geom) > 0.5
            )

            select * from face_matching;

        """.format(shifted_topo_schema=self.shifted_topo_schema, cadastral_topo_schema=self.cadastral_topo_schema,
                   farm_topo_schema=self.farm_topo_schema, schema=self.input_schema)
        
        with pgconn.cursor() as curs:
            curs.execute(sql_query)
            face_matching=curs.fetchall()
        pgconn.commit()

        for (shifted_id, original_id) in face_matching:
            if original_id not in self.face_traversal_order:
                print("Problem! Face ID {original_id} not documented in face traversal order!".format(
                    original_id=original_id))
                continue
            
            face_order=self.face_traversal_order[original_id]
            sql_query="""
                update {schema}.shifted_faces
                set face_order={face_order}
                where face_id={shifted_id}
                ;
            """.format(schema=self.input_schema,
                       face_order=face_order, shifted_id=shifted_id)

            with pgconn.cursor() as curs:
                curs.execute(sql_query)
            pgconn.commit()


        print(face_matching)


    def create_superpolygons(self, pgconn):

        # Comment DropTopology where necessary
        sql_query="""
            drop table if exists {schema}.experimental;
            create table {schema}.experimental as

            with shifted_faces as (
                select * from {schema}.shifted_faces
            ),
            farm_plots as (
                select
                    gid as face_id,
                    geom_32643 as geom
                from
                    {schema}.crop_plots_clipped_dedup
            ),
            face_ownership as (
                select
                    shifted_faces.face_id as shifted_id,
                    farm_plots.face_id as farm_id,
                    farm_plots.geom as farm_geom,
                    shifted_faces.geom as shifted_geom
                from
                    shifted_faces, farm_plots
                where
                    st_intersects(shifted_faces.geom, farm_plots.geom)
                    and
                    st_area(st_intersection(shifted_faces.geom, farm_plots.geom)) /
                        st_area(farm_plots.geom) > 0.7
            ),
            farm_union as (
                select
                    shifted_id as face_id,
                    shifted_geom as shifted_geom,
                    st_buffer(st_buffer(st_union(farm_geom), 20), -20) as combined_geom
                from
                    face_ownership
                group by
                    shifted_id,
                    shifted_geom
            )

            select
                face_id,
                st_union(
                    st_intersection(st_buffer(shifted_geom, 5), combined_geom),
                    st_buffer(shifted_geom, 0)
                ) as geom
            from
                farm_union
            ;

            select DropTopology('{experimental_topo}');
            select CreateTopology('{experimental_topo}', 32643);

            select st_createtopogeo('{experimental_topo}', geom)
            from (
                select
                    st_collect(st_simplifypreservetopology(geom, 10)) as geom
                from
                    {schema}.experimental
            ) as foo;
        """.format(schema=self.input_schema, experimental_topo=self.experimental_topo_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()
    

    def establish_node_order(self, pgconn):
        sql_query="""
            drop table if exists {schema}.shifted_nodes_debug;
            create table {schema}.shifted_nodes_debug (
                geom geometry(Point, 32643),
                node_id int generated by default as identity,
                node_order int default 999
            );
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()

        for node_id in self.node_order.keys():
            node_order=self.node_order[node_id]
            sql_query="""
                select geom from {schema}.shifted_nodes where node_id={node_id};
            """.format(schema=self.input_schema, node_id=node_id)

            with pgconn.cursor() as curs:
                curs.execute(sql_query)
                geom=curs.fetchone()[0]

            sql_query="""
                insert into {schema}.shifted_nodes_debug
                values('{geom}'::geometry(Point, 32643), {node_id}, {node_order})
            """.format(schema=self.input_schema, geom=geom, node_id=node_id, node_order=node_order)

            with pgconn.cursor() as curs:
                curs.execute(sql_query)
            pgconn.commit()
    
    
    def shift_faces_to_debug(self, pgconn):
        sql_query="""
            drop table if exists {schema}.debug_faces;
            create table {schema}.debug_faces as

            select * from {schema}.shifted_faces;
        """.format(schema=self.input_schema)

        with pgconn.cursor() as curs:
            curs.execute(sql_query)
        pgconn.commit()



# Should take 10-30 seconds to run for any village
if __name__=="__main__":

    start_timer=time.time()

    print("------ESTABLISHING CONNECTION------")

    ###### EDIT THIS ######
    pgconn_obj = PGConn(
        "localhost",
        5432,
        "pocra",
        "postgresql_user",
        "postgresql_password")
    
    pgconn=pgconn_obj.connection()

    cur_village="sawangi"

    if cur_village=="barhanpur":
        vec=Vectorial_Map(pgconn, input_schema="barhanpur", farm_topo_schema="barhanpur_farm_topo",
            cadastral_topo_schema="barhanpur_cadastral_topo", shifted_topo_schema="barhanpur_shifted_topo")
        gpd_file="barhanpur_ref_nodes.gpkg"
    elif cur_village=="sawangi":
        vec=Vectorial_Map(pgconn, input_schema="sawangi", farm_topo_schema="sawangi_farm_topo",
            cadastral_topo_schema="sawangi_cadastral_topo", shifted_topo_schema="sawangi_shifted_topo")
        gpd_file="sawangi_ref_nodes.gpkg"
    elif cur_village=="mangrul":
        vec=Vectorial_Map(pgconn, input_schema="anthrokrishi", farm_topo_schema="mangrul_farm_topo",
            cadastral_topo_schema="mangrul_cadastral_topo", shifted_topo_schema="mangrul_shifted_topo")
        gpd_file="mangrul_ref_nodes.gpkg"
    
    gdf=gpd.read_file("reference_nodes/"+gpd_file)
    inserted_nodes=[]
    for i in gdf.index:
        vec.insert_shifted_node(gdf['node_id'][i], gdf['geometry'][i])
        inserted_nodes.append(gdf['node_id'][i])
    
    
    print("------SETTING UP FACE BFS------")
    time.sleep(1)
    vec.setup_face_bfs(pgconn)
    #vec.first_bfs(pgconn, inserted_nodes)

    print("------STARTING BFS------")
    time.sleep(1)
    vec.face_bfs(pgconn, inserted_nodes)
    #vec.second_bfs(pgconn, inserted_nodes)

    print("------MAKING EDGES------")
    time.sleep(1)
    vec.make_edges(pgconn)

    print("------MAKING TOPOLOGY------")
    time.sleep(1)
    vec.make_topology(pgconn)

    print("------EXTRACTING FACES------")
    time.sleep(1)
    vec.extract_faces(pgconn)

    print("------DEBUGGING------")
    vec.fill_faces(pgconn, inserted_nodes)
    vec.make_edges(pgconn)
    vec.extract_faces(pgconn)
    vec.shift_faces_to_debug(pgconn)


    vec.establish_node_order(pgconn)
    face_ratings=vec.shifted_face_rating
    avg_rating_list=[]
    for rating in face_ratings.values():
        avg_rating_list.append(rating)
    avg_rating=sum(avg_rating_list)/len(avg_rating_list)
    print("Average rating:", avg_rating)
    print("Total number of faces covered:", len(vec.shifted_face_rating.keys()))
    print("Number of original faces:", vec.get_num_original_faces(pgconn))
    #vec.create_superpolygons(pgconn)
    #vec.merge_shifted_cadastrals(pgconn)
    
    end_timer=time.time()
    print("Time taken:", end_timer-start_timer)
