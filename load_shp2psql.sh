#!/bin/bash
#Change the srid as required
#cadastral data has srid 32643. 
#farmplots have 32644/4326 srid
load_data(){
    DATABASE_NAME="$1"
    TABLE_NAME="$2"
    shapefile_location="$3"
    shp2pgsql -I -s 4326 "$shapefile_location" "$TABLE_NAME" | psql -U "postgres" -d "$DATABASE_NAME"
}



load_data "$1" "$2" "$3"
