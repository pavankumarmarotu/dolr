
---create topology

select
    dropTopology('cadastral_top3'); -- dont use this when creating topo first time

select
    createTopology('cadastral_top3', 32643);

select
    st_createtopogeo('cadastral_top3', geom)
from
    (
        select
            st_SimplifyPreserveTopology(st_collect(st_force2d(geom)), 0) as geom
        from
            sawangi.survey_region_jiiter --- table name
    ) as foo;
