# Land Records Modernization
This repository contains software for keeping land entitlement records within the state of Maharashtra.  This repository is maintained by project staff at Department of Computer Science and Engineering, IIT Bombay.

## How to contribute

Code changes to this repostory can be proposed by creating a merge request.  Merge requests are reviewed by team members.  Authors of merge requests are expected to incorporate review feedback, if any.  Once a merge request is approved, the it can be merged in the `main` branch.  The workflow is as follows.

1. Pull the latest code from upstream
```
git pull origin main
```
2. Create a new branch from `main`
```
git checkout -b my-fancy-idea
```
3. Implement your code changes by making new commits to `my-fancy-idea` branch.  Test them, preferably by added new tests.
4. It's time to create a commit.  Group logical changes into separate commits.  Use elaborate commit messages to describe your change so as to aid the reviewers.  Steps to commit a logical change are as follows.
```bash
# inspect files changed
git status

# add hunks from changed files one by one, inspecting each hunk
# carefully
git add -p

# create a commit
git commit

# check if the commit log is as expected
git log
```
5. Push the branch upstream (to gitlab.com) so that a merge request can be created.
```
git push origin my-fancy-idea
```
6. Create merge request by following the [GitLab web interface](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).  Set the branch `my-fancy-idea` as source of the merge request and the traget branch as `main`.

7. Incorporate review feedback by making new changes and committing them as in steps 3 to 5.
8. Once the merge request is approved, the `my-fancy-idea` is ready to be merged into `main`
```bash
# inspect commit log on my-fancy-idea branch before merging
git log my-fancy-idea

# pull the latest changes from main
git checkout main
git pull --reabse origin main

# rebase my-fancy-idea against the latest changes pulled into main
git checkout my-fancy-idea
git rebase main

# merge my-fancy-idea into main
git checkout main
git merge --ff-only my-fancy-idea

# The main branch in your local repository now has the desired commits
# from my-fancy-idea branch.  Test the modified main branch, and if
# anything is wrong, fix it by making new commits to my-fancy-idea
# branch.  Only when the modified main branch is found to behave as
# expected, proceed to push it upstream as follows.

# inspect the commit range reported by the previous command
git log <paste the commit range here>

# The commit log should contain only the newly added commits from the 
# merge request.  If so, run the push command without the --dry-run flag.
git push origin main
```

### How to add new tests
TBD

## Project description
TBD

## Usage
TBD

