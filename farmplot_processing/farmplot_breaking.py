import math
import psycopg2
from scipy.optimize import minimize 


def connect_to_DB():
    DB_HOST = "localhost"
    DB = "govt"
    DB_USER = "postgres"
    DB_PASSWORD = "cooking"
    try:
        conn = psycopg2.connect(
        host = DB_HOST,
        database = DB,
        user = DB_USER,
        password = DB_PASSWORD)
        conn.autocommit = True
        print("DB Connected")
        return conn.cursor()
    except:
        print("Connection failed")

curr = connect_to_DB()

def get_points(lines):
    line = lines[11:-2].split(',')
    p1 = line[0].split(' ')
    p2 = line[1].split(' ')
    point1 =[float(i) for i in p1]
    point2 = [float(i) for i in p2]
    return [point1,point2]


def get_bisector_points(str):
    lines = str.split(" - ")
    assert len(lines) == 2, "get_vs returned wrong string format"
    l1points = get_points(lines[0])
    l2points = get_points(lines[1])
    pointA = l1points[0]
    pointB = [(l1points[1][0] + l2points[0][0])/2,(l1points[1][1] + l2points[0][1])/2]
    pointC = l2points[1]
    bisector_point2 = pointB #common point
    distAB = math.sqrt((pointA[0]-pointB[0])**2+(pointA[1]-pointB[1])**2)
    distCB = math.sqrt((pointC[0]-pointB[0])**2+(pointC[1]-pointB[1])**2)
    pointD = [(distAB * pointA[0] + distCB * pointC[0]) / (distAB + distCB),(distAB * pointA[1] + distCB * pointC[1]) / (distAB + distCB)]
    bisector_point1 = (pointB)
    bisector_point2 = (pointD)
    return [bisector_point1,bisector_point2]

def break_farmplots(str,gid):
    points = get_bisector_points(str)
    sql = f'''
            select st_astext(st_split(st_transform(p.geom,32643),sub5.geom))from
            (select st_geomfromtext(txt,{32643}) as geom from 
            (select st_astext(geom) as txt from
            (
            SELECT ST_MakeLine(ST_TRANSLATE(a, sin(az1) * len, cos(az1) * 
            len),b) as geom

            FROM (
                SELECT a, b, ST_Azimuth(b,a) AS az1, ST_Distance(a,b) + 500 AS len
                FROM (
                    SELECT ST_StartPoint(geom) AS a, ST_EndPoint(geom) AS b
                    FROM ST_MakeLine(ST_MakePoint({points[0][0]},{points[0][1]}), 
                    ST_MakePoint({points[1][0]},{points[1][1]})) AS geom
                ) AS sub
            ) AS sub2
            ) AS sub3) AS sub4) sub5, {farmplots} as p where p.gid = {gid}
            '''
    curr.execute(sql)
    a = curr.fetchall()
    """First 26 char GEOMETRYCOLLECTION(POLYGON"""
    poly = a[0][0][26: -1].split(',POLYGON')

    for polygon in poly:
        polygon = 'POLYGON' + polygon
        print(polygon)
        sql = f'''
        insert into {village}.t1(geom)  
        select (select st_transform(st_geomfromtext('{polygon}',32643),4326))'''
        curr.execute(sql)


def find_vs_and_break(gid):
    sql = f'''
        select gid,
        polygon_num,
        point_order as vertex,
        --
        case when point_order = 1
            then last_value(ST_Astext(ST_Makeline(sp,ep))) over (partition by gid, polygon_num)
            else lag(ST_Astext(ST_Makeline(sp,ep)),1) over (partition by gid, polygon_num order by point_order)
        end ||' - '||ST_Astext(ST_Makeline(sp,ep)) as lines,
        --
        abs(abs(
        case when point_order = 1
            then last_value(degrees(ST_Azimuth(sp,ep))) over (partition by gid, polygon_num)
            else lag(degrees(ST_Azimuth(sp,ep)),1) over (partition by gid, polygon_num order by point_order)
        end - degrees(ST_Azimuth(sp,ep))) -180 ) as ang
    from (-- 2.- extract the endpoints for every 2-point line segment for each linestring
        --     Group polygons from multipolygon
        select gid,
                coalesce(path[1],0) as polygon_num,
                generate_series(1, ST_Npoints(geom)-1) as point_order,
                ST_Pointn(geom, generate_series(1, ST_Npoints(geom)-1)) as sp,
                ST_Pointn(geom, generate_series(2, ST_Npoints(geom)  )) as ep
        from ( -- 1.- Extract the individual linestrings and the Polygon number for later identification
                select gid,
                    (ST_Dump(ST_Boundary(st_transform(geom,32643)))).geom as geom,
                    (ST_Dump(ST_Boundary(st_transform(geom,32643)))).path as path -- To identify the polygon
                from {village}.t2 where gid = {gid} ) as pointlist ) as segments;
    '''
    curr.execute(sql)
    a = curr.fetchall()
    for x in a:
        print(x)
        if(x[-1] > 355 or x[-1] < 5):
            break_farmplots(x[-2],gid)
    

village = 'sawangi'
farmplots = f'{village}.farmplots'
farmplots_original = f'{village}.farmplots_original'

def t1_table():
    sql = f'''drop table if exists {village}.t1;
    create table {village}.t1 (id serial,geom geometry(Polygon, 4326))'''
    curr.execute(sql)

t1_table()

def all_gid():
    sql = f'''
           select gid from {farmplots}; '''
    curr.execute(sql)
    a = curr.fetchall()
    for x in a:
        find_vs_and_break(x[0])

# all_gid()
#987db7
#1329 gid
find_vs_and_break(5159)

sql = '''create table bori.farmplots as select ogc_fid as gid, 
description as type, wkb_geometry as geom 
from boribudruk where description != 'trees';'''