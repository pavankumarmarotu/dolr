import os
import subprocess
import shutil
import math
import psycopg2
from scipy.optimize import minimize 


def connect_to_DB():
    DB_HOST = "localhost"
    DB = "govt"
    DB_USER = "postgres"
    DB_PASSWORD = "cooking"
    try:
        conn = psycopg2.connect(
        host = DB_HOST,
        database = DB,
        user = DB_USER,
        password = DB_PASSWORD)
        conn.autocommit = True
        print("DB Connected")
        return conn.cursor()
    except:
        print("Connection failed")

curr = connect_to_DB()

def remove_trees():
    sql = f'''create table {farmplots} as 
            select * from {farmplots_original} where type != 'trees'; '''
    curr.execute(sql)

def find_varp(gid):
    sql = f'''
        select gid,
       polygon_num,
       point_order as vertex,
       --
       case when point_order = 1
         then last_value(ST_Astext(ST_Makeline(sp,ep))) over (partition by gid, polygon_num)
         else lag(ST_Astext(ST_Makeline(sp,ep)),1) over (partition by gid, polygon_num order by point_order)
       end ||' - '||ST_Astext(ST_Makeline(sp,ep)) as lines,
       --
       abs(abs(
       case when point_order = 1
         then last_value(degrees(ST_Azimuth(sp,ep))) over (partition by gid, polygon_num)
         else lag(degrees(ST_Azimuth(sp,ep)),1) over (partition by gid, polygon_num order by point_order)
       end - degrees(ST_Azimuth(sp,ep))) -180 ) as ang
from (-- 2.- extract the endpoints for every 2-point line segment for each linestring
      --     Group polygons from multipolygon
      select gid,
             coalesce(path[1],0) as polygon_num,
             generate_series(1, ST_Npoints(geom)-1) as point_order,
             ST_Pointn(geom, generate_series(1, ST_Npoints(geom)-1)) as sp,
             ST_Pointn(geom, generate_series(2, ST_Npoints(geom)  )) as ep
      from ( -- 1.- Extract the individual linestrings and the Polygon number for later identification
             select gid,
                    (ST_Dump(ST_Boundary(geom))).geom as geom,
                    (ST_Dump(ST_Boundary(geom))).path as path -- To identify the polygon
              from {farmplots} where gid = {gid} ) as pointlist ) as segments;
    '''
    curr.execute(sql)
    a = curr.fetchall()
    sum = 0 
    for x in a:
        print(type(x[-2]))
        pi = math.pi
        theta = math.radians(float(x[-1]))
        theta_c = min(theta,6.28-theta)
        sum  = sum + pi - theta_c
    return sum/(2*pi)

village = 'sawangi'
farmplots = f'{village}.farmplots'
farmplots_original = f'{village}.farmplots_original'

# print(find_varp(2532))
find_varp(2532)